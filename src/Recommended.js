import React from 'react'
import './Recommended.css'
import VideoCard from './Video'
function Recommended() {
    return (
        <div className="recommened">
            <h2>Recommended</h2>
            <div className="recommended_videos">
             <VideoCard 
             img1="https://i.ytimg.com/an_webp/15R2pMqU2ok/mqdefault_6s.webp?du=3000&sqp=CP-pjZEG&rs=AOn4CLAEEHPV7EUjRgyQiRqDl2P-pJJBUg"
             img2="https://yt3.ggpht.com/ytc/AKedOLQw1l7gHFAf1h646J-ofb8quKz7FUQPMV-lzmMx_w=s176-c-k-c0x00ffffff-no-rj-mo"
             title="Huberman Lab Podcast #61"
             channel_name="Andrew Huberman"
            views="87K views"
            timestamp="Streamed 1 month ago"
            />
             <VideoCard 
             img1="https://i.ytimg.com/vi/GK2Fno58_do/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLDu1FJAfzG0unwonpwlBF6h0EygIA"
             img2="https://yt3.ggpht.com/S98j7tcPW2-3DUltlXtC9QfyFKyNLEVjcvnB-3Y8xreDu3QxyhzpEvxsJLguCbKsuE5CrvLRNPU=s48-c-k-c0x00ffffff-no-rj"
             title="Piya O re Piya"
             channel_name="HEAVEN"
            views="659K views"
            timestamp="9 month ago"
            />
             <VideoCard 
             img1="https://i.ytimg.com/an_webp/s9iIXZoEFbk/mqdefault_6s.webp?du=3000&sqp=CNaljZEG&rs=AOn4CLBDDH0jNfl3qMwBsYgpPIEPh5QNwQ"
             img2="https://yt3.ggpht.com/a-/AOh14GhxJX4-guhPSZJu9zwk6lhFsP66FKIZZMnGVA=s68-c-k-c0x00ffffff-no-rj-mo"
             title="10 Amazing People from 10 Countries"
             channel_name="Nas Daily"
            views="139K views"
            timestamp="18 hours ago"
            />
             <VideoCard 
             img1="https://i.ytimg.com/an_webp/_SHf-WJhnCo/mqdefault_6s.webp?du=3000&sqp=CITCjZEG&rs=AOn4CLCt2ymG4wZV1u3niASWbpXMcRtw0Q"
             img2="https://yt3.ggpht.com/a-/AOh14GjzTZDLzyKAPGRylyosPBYQB5KshiIFbaenvw=s68-c-k-c0x00ffffff-no-rj-mo"
             title="Sunder Pichai Success Story in Hindi |Google CEO"
             channel_name="TheSutraas Hindi"
            views="817K views"
            timestamp="2 month ago"
            />

            <VideoCard 
             img1="https://i.ytimg.com/an_webp/s9iIXZoEFbk/mqdefault_6s.webp?du=3000&sqp=CNaljZEG&rs=AOn4CLBDDH0jNfl3qMwBsYgpPIEPh5QNwQ"
             img2="https://yt3.ggpht.com/a-/AOh14GhxJX4-guhPSZJu9zwk6lhFsP66FKIZZMnGVA=s68-c-k-c0x00ffffff-no-rj-mo"
             title="10 Amazing People from 10 Countries"
             channel_name="Nas Daily"
            views="139K views"
            timestamp="18 hours ago"
            />
             <VideoCard 
             img1="https://i.ytimg.com/an_webp/_SHf-WJhnCo/mqdefault_6s.webp?du=3000&sqp=CITCjZEG&rs=AOn4CLCt2ymG4wZV1u3niASWbpXMcRtw0Q"
             img2="https://yt3.ggpht.com/a-/AOh14GjzTZDLzyKAPGRylyosPBYQB5KshiIFbaenvw=s68-c-k-c0x00ffffff-no-rj-mo"
             title="Sunder Pichai Success Story in Hindi |Google CEO"
             channel_name="TheSutraas Hindi"
            views="817K views"
            timestamp="2 month ago"
            />
           
             <VideoCard 
             img1="https://i.ytimg.com/an_webp/15R2pMqU2ok/mqdefault_6s.webp?du=3000&sqp=CP-pjZEG&rs=AOn4CLAEEHPV7EUjRgyQiRqDl2P-pJJBUg"
             img2="https://yt3.ggpht.com/ytc/AKedOLQw1l7gHFAf1h646J-ofb8quKz7FUQPMV-lzmMx_w=s176-c-k-c0x00ffffff-no-rj-mo"
             title="Huberman Lab Podcast #61"
             channel_name="Andrew Huberman"
            views="87K views"
            timestamp="Streamed 1 month ago"
            />
           
           
           
             <VideoCard 
             img1="https://i.ytimg.com/vi/GK2Fno58_do/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLDu1FJAfzG0unwonpwlBF6h0EygIA"
             img2="https://yt3.ggpht.com/S98j7tcPW2-3DUltlXtC9QfyFKyNLEVjcvnB-3Y8xreDu3QxyhzpEvxsJLguCbKsuE5CrvLRNPU=s48-c-k-c0x00ffffff-no-rj"
             title="Piya O re Piya"
             channel_name="HEAVEN"
            views="659K views"
            timestamp="9 month ago"
            />

            <VideoCard 
             img1="https://i.ytimg.com/an_webp/s9iIXZoEFbk/mqdefault_6s.webp?du=3000&sqp=CNaljZEG&rs=AOn4CLBDDH0jNfl3qMwBsYgpPIEPh5QNwQ"
             img2="https://yt3.ggpht.com/a-/AOh14GhxJX4-guhPSZJu9zwk6lhFsP66FKIZZMnGVA=s68-c-k-c0x00ffffff-no-rj-mo"
             title="10 Amazing People from 10 Countries"
             channel_name="Nas Daily"
            views="139K views"
            timestamp="18 hours ago"
            />
             <VideoCard 
             img1="https://i.ytimg.com/an_webp/_SHf-WJhnCo/mqdefault_6s.webp?du=3000&sqp=CITCjZEG&rs=AOn4CLCt2ymG4wZV1u3niASWbpXMcRtw0Q"
             img2="https://yt3.ggpht.com/a-/AOh14GjzTZDLzyKAPGRylyosPBYQB5KshiIFbaenvw=s68-c-k-c0x00ffffff-no-rj-mo"
             title="Sunder Pichai Success Story in Hindi |Google CEO"
             channel_name="TheSutraas Hindi"
            views="817K views"
            timestamp="2 month ago"
            />
            
              <VideoCard 
             img1="https://i.ytimg.com/an_webp/15R2pMqU2ok/mqdefault_6s.webp?du=3000&sqp=CP-pjZEG&rs=AOn4CLAEEHPV7EUjRgyQiRqDl2P-pJJBUg"
             img2="https://yt3.ggpht.com/ytc/AKedOLQw1l7gHFAf1h646J-ofb8quKz7FUQPMV-lzmMx_w=s176-c-k-c0x00ffffff-no-rj-mo"
             title="Huberman Lab Podcast #61"
             channel_name="Andrew Huberman"
            views="87K views"
            timestamp="Streamed 1 month ago"
            />
            
            
             <VideoCard 
             img1="https://i.ytimg.com/vi/GK2Fno58_do/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG&rs=AOn4CLDu1FJAfzG0unwonpwlBF6h0EygIA"
             img2="https://yt3.ggpht.com/S98j7tcPW2-3DUltlXtC9QfyFKyNLEVjcvnB-3Y8xreDu3QxyhzpEvxsJLguCbKsuE5CrvLRNPU=s48-c-k-c0x00ffffff-no-rj"
             title="Piya O re Piya"
             channel_name="HEAVEN"
            views="659K views"
            timestamp="9 month ago"
            />

            </div>
         </div>
    )
}

export default Recommended
